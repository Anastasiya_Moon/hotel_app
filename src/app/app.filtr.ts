// import { Component, ViewChild } from '@angular/core';
// import { Nav, Platform } from 'ionic-angular';
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';
//
// import { HomePage } from '../pages/home/home';
// import { HotelsPage } from '../pages/hotels/hotels';
//
// // @Component({
// //   templateUrl: 'app.html'
// // })
// module MyApp {
//
//   "use strict";
//
//   export interface IMyService {
//   myMethod(param: string);
// }
//
// class MyService implements IMyService {
//
//   static $inject = ["Dependency"];
//
//   constructor(private dependency: IDependency) {
//   }
//
//   myMethod(param: string) {
//     return this.dependency.field + param;
//   }
//
// }
//
// // angular.module("services").service("MyService", MyService);
//
// }
//
