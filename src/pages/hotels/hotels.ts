import {Component, ViewChild} from '@angular/core';
import {Nav, NavController, NavParams} from 'ionic-angular';

import {HotelPage} from '../hotel/hotel';
import {FilterPage} from '../filter/filter';

//3)interface type Hotel
interface Hotel {
    imageUrl: string,
    title: string,
    description: string,
    roomCost: number,
    hasParking: boolean,
    address: string,
    phone: string
}

@Component({
    selector: 'page-hotels',
    templateUrl: 'hotels.html'
})

export class HotelsPage {
    @ViewChild(Nav) nav: Nav;

    hotels: Array<Hotel>;
    defaultHotels: Array<Hotel>;

    constructor(public navParams: NavParams, public navCtrl: NavController) {
        this.defaultHotels = [
            {
                imageUrl: 'https://img.gazeta.ru/files3/837/4860837/hotel-pic668-668x444-62402.jpg',
                title: 'Будапешт',
                description: 'Московский отель "Будапешт"',
                roomCost: 5000,
                hasParking: true,
                address: 'Москва, ул. Петровские Линии, 2',
                phone: '8 (495) 729-35-01'
            },
            {
                imageUrl: 'https://cdn.ostrovok.ru/t/640x400/extranet/50/1c/501c6211826d67319ab8503185fa4032ef4eafb2.jpeg',
                title: 'Космос',
                description: 'Гостиница "Космос"',
                roomCost: 3000,
                hasParking: false,
                address: 'Москва, пр-т Мира, 150',
                phone: '8 (495) 234-12-06'
            },
            {
                imageUrl: 'https://cdn.ostrovok.ru/t/640x400/mec/90/38/90386dfbdf26ae8b7eaaed1e29cf1355fa8da0f8.jpeg',
                title: 'Genius Hotel Downtown ',
                description: 'Гостиница "Downtown"',
                roomCost: 10960,
                hasParking: true,
                address: 'Via Porlezza 4, Милан',
                phone: '+7 499 681-03-33'
            },
            {
                imageUrl: 'https://cdn.ostrovok.ru/t/640x400/content/20/f7/20f7d5a599d17e536b2e1e7ae27ecf63e718a2a4.jpeg',
                title: 'Hotel Dei Cavalieri',
                description: 'Гостиница "Dei Cavalieri"',
                roomCost: 13618,
                hasParking: true,
                address: 'Piazza Giuseppe Missori 1, Милан',
                phone: '8 (495) 234-12-06'
            },
            {
                imageUrl: 'https://cdn.ostrovok.ru/t/640x400/second/4a/6a/4a6a0461413a077620a5776020233131209d4cc1.jpeg',
                title: 'Ostello Bello ',
                description: 'Гостиница "Ostello Bello"',
                roomCost: 12248,
                hasParking: false,
                address: 'Via Medici 4, Милан',
                phone: '+7 499 681-03-33'
            }
        ];
        this.hotels = this.defaultHotels;
        if (navParams.get('hotels')) {
            this.hotels = navParams.get('hotels');
        }
        if (navParams.get('cancel')) {
            this.hotels = this.defaultHotels;
        }
    }

    //2) open page one hotel
    openPage(hotels): void {
        this.navCtrl.push(HotelPage, {
            'hotels': hotels
        });
    }

    openFilter(): void {
        this.navCtrl.push(FilterPage, {
            'hotels': this.defaultHotels
        });
    };
}