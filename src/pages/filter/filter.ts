import {Component, ViewChild} from '@angular/core';
import {Nav, NavController, NavParams} from 'ionic-angular';
import * as $ from "jquery";

import {HotelsPage} from "../hotels/hotels";

@Component({
    selector: 'page-filter',
    templateUrl: 'filter.html'
})

export class FilterPage {
    @ViewChild(Nav) nav: Nav;
    hotels: string;
    form = {};

    constructor(public navParams: NavParams, public navCtrl: NavController) {
        this.hotels = navParams.get('hotels');
    };

    //2)filter
    filter(): void {
        let hotels = this.hotels;
        let min = this.form['min'];
        let max = this.form['max'];
        let parking: boolean = false;
        let reg = /^[1-9]\d*$/;
        if (!reg.test(min) || !reg.test(max)) {
            $('.exep').text('Укажите два числовых значения больше 0!');
        } else {
            if ((this.form['parking']) == true) {
                parking = this.form['parking'];
            }
            let list = [];
            for (let i = 0; i < hotels.length; i++) {
                let hotel = hotels[i];
                if (hotel['roomCost'] >= min && hotel['roomCost'] <= max) {
                    if (parking) {
                        if (hotel['hasParking'] == parking)
                            list.push(hotel);
                    } else list.push(hotel);
                }
            }
            if (list.length == 0)
            {
                console.log(234)
                $('.exep').text('По вашему запросу нет данных!');
            }else {
                this.navCtrl.push(HotelsPage, {
                    'hotels': list
                });
            }

        }
    };

    cancel(): void {
        this.navCtrl.push(HotelsPage, {
            'cancel': true
        });
    };

    closePage(): void {
        this.navCtrl.push(HotelsPage, {
            'cancel': true
        });
    };
}