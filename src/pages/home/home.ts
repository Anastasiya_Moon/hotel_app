import {Component, ViewChild} from '@angular/core';
import {Nav, NavController} from 'ionic-angular';

import { HotelsPage } from '../hotels/hotels';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  @ViewChild(Nav) nav: Nav;

  constructor(public navCtrl: NavController) {

  }

  //1) open page all hotels
  openPage(): void {
    this.navCtrl.push(HotelsPage);
  }

}
