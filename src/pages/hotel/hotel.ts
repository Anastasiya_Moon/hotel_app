import {Component, ViewChild} from '@angular/core';
import {NavParams} from 'ionic-angular';

@Component({
    selector: 'page-hotel',
    templateUrl: 'hotel.html'
})

export class HotelPage {
    hotel: string;
    parking: string;
    val = "руб";

    constructor(public navParams: NavParams) {
        this.hotel = navParams.get('hotels');
        // @ts-ignore
        this.parking = this.hotel.hasParking ? "есть" : "отсутствует";
        // @ts-ignore
        console.log(this.hotel.hasParking, this.hotel.title, this.hotel  )
    }
}

